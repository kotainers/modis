process.env.NODE_PATH = __dirname;
require('module').Module._initPaths();
var config = require('config');
var colors = require('colors');
var WebSocketServer = new require('ws');
var auth = require('lib/auth');
var user_module = require('lib/user');
var cache = require('lib/caching');
var email_ver = require('lib/email_verification');
var fs = require('fs');
// Клиент для кеша
var rediska = require('lib/connection').rediska;
// Старт задач
var task = require('lib/tasks');
// Модуль старосты
var steward = require('lib/steward');
// Модуль методиста
var metodist = require('lib/metodist');
// Модуль преподавателя
var prepod = require('lib/prepod');
// Модуль состояния сервера
var status = require('lib/status_cheker');
global.__approot = __dirname;
// подключенные клиенты
var online = 0;
var now = new Date();

// WebSocket-сервер на порту8081
var webSocketServer = new WebSocketServer.Server({port: config.get('port')});

webSocketServer.on('connection', function(ws) {
    var user = ws;
    user.token = '';

    ws.on('message', function(message) {
        now = new Date();
        try{
            var msg = JSON.parse(message);
            console.log(now.getHours() + ':' + now.getMinutes() + ' Действие: '.magenta + message);
            // Проверка статуса сервера
            if (msg.act =='getStatus')
            {
                status.getStatus(user);
            }
            // Отдаём страницу логина
            if (msg.act =='getLoginPage')
            {
                rediska.get('login-page' , function (err, repl) {
                    user.send(repl);
                });
            };
            // Регистрация нового пользователя
            if (msg.act =='register')
            {
                auth.Register(user, msg.login, msg.password, msg.nStudentCard, __dirname, msg.email);
            };
            // Восстановление пароля
            if (msg.act =='restore')
            {
                user_module.restorePassword(user, msg);
            };
            // Авторизация пользователя
            if (msg.act =='login')
            {
                auth.Check(user, msg.login, msg.password, __dirname,function(){});
            };
            // Выход из ситемы
            if (msg.act == 'logOut')
            {
                auth.LogOut(msg.login);
                rediska.get('login-page' , function (err, repl) {
                    user.send(repl);
                });
            };
            // Авторизация по токену
            if (msg.act =='login-token')
            {
                auth.CheckToken(user, msg.login, msg.token, __dirname,function(){});
            };
            //
            if (msg.act == 'updateUserEmail'){
                user_module.updateUserEmail(user, msg.login, msg.email,__dirname);
            };
            if(msg.act == 'load'){
                rediska.get('load' , function (err, repl) {
                    user.send(repl);
                });
            };

            if (msg.act == 'disciplineAttendanceDetails'){
                user_module.disciplineAttendanceDetails(user, msg,__dirname);
            };

            if (msg.act == 'disciplineProgressDetails'){
                user_module.disciplineProgressDetails(user, msg,__dirname);
            };

            // Студент
            if(msg.act == 'getMoreForDiscipline'){
                user_module.getMoreForDiscipline(user, msg, __dirname);
            }
            //--
            //Cтароста
            if(msg.act == 'getLessonByDate'){
                if(msg.token == user.token) {
                    steward.getLessonByDate(user, msg, __dirname);
                }
            }
            if(msg.act == 'getAttendanceForLesson'){
                if(msg.token == user.token) {
                    steward.getAttendanceForLesson(user, msg, __dirname);
                }
            }
            if(msg.act == 'changeAttendanceForStudent'){
                if(msg.token == user.token) {
                    steward.changeAttendanceForStudent(user, msg);
                }
            }
            //--
            //Методист
            if(msg.act == 'getMoreForSpecialty'){
                if(msg.token == user.token){
                    metodist.getMoreForSpecialty(user, msg, __dirname);
                }
            }
            if(msg.act == 'getMoreForSpecialtyMove'){
                if(msg.token == user.token){
                    metodist.getMoreForSpecialtyMove(user, msg, __dirname);
                }
            }
            if(msg.act == 'listStudentsInGroup'){
                if(msg.token == user.token){
                    metodist.listStudentsInGroup(user, msg, __dirname);
                }
            }
            if(msg.act == 'getStudentDiscipline'){
                if(msg.token == user.token){
                    metodist.getStudentDiscipline(user, msg, __dirname);
                }
            }
            if(msg.act == 'getMoreForDisciplineMetod'){
                if(msg.token == user.token){
                    metodist.getMoreForDisciplineMetod(user, msg, __dirname);
                }
            }
            if(msg.act == 'find'){
                if(msg.token == user.token){
                    metodist.find(user, msg, __dirname);
                }
            }
            if(msg.act == 'moveToQuarantine'){
                if(msg.token == user.token){
                    metodist.moveToQuarantine(user, msg);
                }
            }
            if(msg.act == 'addNewGroup'){
                if(msg.token == user.token){
                    metodist.addNewGroup(user, msg);
                }
            }
            if(msg.act == 'moveStudentInto'){
                if(msg.token == user.token){
                    metodist.moveStudentInto(user, msg, __dirname);
                }
            }
            if(msg.act == 'moveStudentIntoGroup'){
                if(msg.token == user.token){
                    metodist.moveStudentIntoGroup(user, msg);
                }
            }
            if(msg.act == 'getStudentCard'){
                if(msg.token == user.token){
                    metodist.getStudentCard(user, msg, __dirname);
                }
            }
            if(msg.act == 'saveStudentMeta'){
                if(msg.token == user.token){
                    metodist.saveStudentMeta(user, msg);
                }
            }
            if(msg.act == 'assignSteward'){
                if(msg.token == user.token){
                    metodist.assignSteward(user, msg);
                }
            }
            if(msg.act == 'takeOffSteward'){
                if(msg.token == user.token){
                    metodist.takeOffSteward(user, msg);
                }
            }
            if(msg.act == 'disciplineAttendanceDetailsMetod'){
                    metodist.disciplineAttendanceDetailsMetod(user, msg, __dirname);
            }
            //Препод
            if(msg.act == 'getDiscipCP'){
                if(msg.token == user.token){
                    prepod.getDiscipCP(user, msg, __dirname);
                }
            }
            if(msg.act == 'listStudentsInGroupPrepod'){
                if(msg.token == user.token){
                    prepod.listStudentsInGroupPrepod(user, msg, __dirname);
                }
            }
            if(msg.act == 'getGroupForCP'){
                if(msg.token == user.token){
                    prepod.getGroupForCP(user, msg, __dirname);
                }
            }
            if(msg.act == 'addCP'){
                if(msg.token == user.token){
                    prepod.addCP(user, msg, __dirname);
                }
            }
            if(msg.act == 'add_new_cp'){
                if(msg.token == user.token){
                    prepod.addNewCP(user, msg);
                }
            }
            if(msg.act == 'changeProgressValue'){
                if(msg.token == user.token){
                    prepod.changeProgressValue(user, msg);
                }
            }
            if(msg.act == 'editCP'){
                if(msg.token == user.token){
                    prepod.editCP(user, msg, __dirname);
                }
            }
            if(msg.act == 'openEditCP'){
                if(msg.token == user.token){
                    prepod.openEditCP(user, msg, __dirname);
                }
            }
            if(msg.act == 'generateCurrentAttestation'){
                if(msg.token == user.token){
                    prepod.generateCurrentAttestation(user, msg, __dirname);
                }
            }
            if(msg.act == 'prepodEditAtt'){
                if(msg.token == user.token){
                    prepod.prepodEditAtt(user, msg, __dirname);
                }
            }
            if(msg.act == 'getAttendanceForLessonPr'){
                if(msg.token == user.token){
                    prepod.getAttendanceForLessonPr(user, msg, __dirname);
                }
            }
        }
        catch(e) {
            console.error(e);
        }
    })

    ws.on('close', function() {
        delete user;
    });
});

console.log(now.getHours() + ':' + now.getMinutes() + " | Сервер приложения запущен на порте: ".green + config.get('port'));