var login = document.getElementById("input-login");
var password = document.getElementById("input-password");
var login_b = document.getElementById("login_b");
var nStudentCard = document.getElementById("input-nStudentCard");
var login_r = document.getElementById("input-login_r");
var password_r = document.getElementById("input-password_r");
var reg = document.getElementById("reg");
var email = document.getElementById("emailsignup");
var r_b = document.getElementById("restore_pass");
var r_h = document.getElementById("to_login_h");
var restore_button = document.getElementById("restore_button");
var email_restore = document.getElementById("emailrestore");
login_b.onclick = function() {
    var event = {
        act: "login",
        login: login.value,
        password: password.value
    };
    var str = JSON.stringify(event);
    ws.send(str);
};

restore_button.onclick = function() {
    var event = {
        act: "restore",
        email: email_restore.value,
    };
    var str = JSON.stringify(event);
    ws.send(str);
    document.getElementById("login").style.zIndex = "22";
    document.getElementById("restore").style.opacity = "0";
};

reg.onclick = function() {
    var event = {
        act: "register",
        login: login_r.value,
        password: password_r.value,
        nStudentCard: nStudentCard.value,
        email: email.value
    };
    var str = JSON.stringify(event);
    ws.send(str);
}
r_b.onclick = function(){
    document.getElementById("login").style.zIndex = "-99";
    document.getElementById("restore").style.opacity = "1";
}
r_h.onclick = function(){
    document.getElementById("login").style.zIndex = "22";
    document.getElementById("restore").style.opacity = "0";
}
