/**
 * Created by kotainer on 15.07.2015.
 */
function sendLesson(lesson_id){
    var event = {
        act: "getAttendanceForLesson",
        lesson_id: lesson_id,
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
};

function changeResult(result){
    console.log(result);
    var now = new Date();
    var temp = document.getElementById("date_modis").value.split('.');
    var sendDate = new Date(temp[2],temp[1] -1 ,temp[0]);
    //Разница во времени
    var dateDifference = now.getTime() - sendDate.getTime();
    //Дата, созданная из остатка времени
    var remainsDate = new Date(dateDifference);
    var remainsSec = (parseInt(remainsDate / 1000));
    var remainsFullDays = (parseInt(remainsSec / (24 * 60 * 60)));
    //if(remainsFullDays <= 7) {
        var itog;
        if (result.checked) {itog = 1;}
        else {itog = 0}
        var event = {
            act: "changeAttendanceForStudent",
            attendance_id: result.id,
            result : itog,
            token: localStorage.getItem('modis-token')
        };
        var str = JSON.stringify(event);
        ws.send(str);
    /*}
    else {
        if (result.checked) {
            result.removeAttribute('checked'); // снять галочку
        }
        else{
            result.checked = true; // поставить галочку
        }
        var notification = new Notification('МОДИС', {
            lang: 'ru-RU',
            body: 'Нельзя изменить поcещаемость старше недели!',
            icon: 'http://37.75.251.12:3000/images/icon.png'
        });
        setTimeout(notification.close.bind(notification), 2000);
    }*/
};
