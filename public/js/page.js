/**
 * Created by kotainer on 04.07.2015.
 */
var b_exit = document.getElementById("b_exit");
b_exit.onclick = function() {
    var event = {
        act: "logOut",
        login: localStorage.getItem('modis-login')
    };
    var str = JSON.stringify(event);
    ws.send(str);
    localStorage.removeItem('modis-token');
    localStorage.removeItem('modis-login');
}

var openDiscipline = function () {
    return function (c) {
        var b = c;
        var event = {
            act: "getMoreForDiscipline",
            id: localStorage.getItem('modis-id'),
            discipline: c
        };
        var str = JSON.stringify(event);
        ws.send(str);
        var a = document.getElementById("item" + b);
        c == b && (a.className = "" == a.className ? "max" : "");
        c != b && (a.className = "", a = document.getElementById("item" + c), a.className = "max", b = c)
    }
}();

var disciplineAttendanceDetails = function(id,discipline){
    var event = {
        act: "disciplineAttendanceDetails",
        login: id,
        discipline: discipline
    };
    var str = JSON.stringify(event);
    ws.send(str);
};

var disciplineAttendanceDetailsMetod = function(id,discipline){
    var event = {
        act: "disciplineAttendanceDetailsMetod",
        login: id,
        discipline: discipline
    };
    var str = JSON.stringify(event);
    ws.send(str);
};

var disciplineProgressDetails = function(id,discipline){
    var event = {
        act: "disciplineProgressDetails",
        login: id,
        discipline: discipline
    };
    var str = JSON.stringify(event);
    ws.send(str);
};