/**
 * Created by kotainer on 07.07.2015.
 */
var openSpecialty = function () {
    return function (c) {
        var b = c;
        var event = {
            act: "getMoreForSpecialty",
            login: localStorage.getItem('modis-login'),
            specialty: c,
            token: localStorage.getItem('modis-token')
        };
        var str = JSON.stringify(event);
        ws.send(str);
        var a = document.getElementById("item_s" + b);
        c == b && (a.className = "" == a.className ? "max" : "");
        c != b && (a.className = "", a = document.getElementById("item_s" + c), a.className = "max", b = c)
    }
}();

var listStudentsInGroup = function(group){
    var event = {
        act: "listStudentsInGroup",
        group: group,
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
};

$(function(){
    //контектное меню для студента
    $.contextMenu({
        selector: '.context-menu-one',
        callback: function(key, options) {
            //var m = "clicked: " + key;
            //window.console && console.log(m) || alert(m);
            switch (key){
                case "assign" :
                        assignSteward($(this).text());
                    break;
                case "delete" :
                        deleteFromGroup($(this).text());
                    break;
                case "move" :
                        moveStudentInto($(this).text());
                    break;
                case "card" :
                        getStudentCard($(this).text());
                    break;
                case "takeOff":
                        takeOffSteward($(this).text());
                    break;
            };
        },
        items: {
            "card": {name: "Карточка студента"},
            "sep2": "---------",
            "assign": {name: "Назначить старостой"},
            "takeOff": {name: "Снять с должности старосты"},
            "sep3": "---------",
            "move": {name: "Переместить в группу"},
            "delete": {name: "Удалить из группы"},
            "sep5": "---------",
            "quit": {name: "Закрыть"}
        }
    });
    //контектное меню для специальности
    $.contextMenu({
        selector: '.context-menu-two',
        callback: function(key, options) {
            //var m = "clicked: " + key;
            //window.console && console.log(m) || alert(m);
            switch (key){
                case "add" :
                    addGroup($(this).text());
                    break;
            };
        },
        items: {
            "add": {name: "Добавить группу"},
            "edit": {name: "Редактировать"},
            "sep1": "---------",
            "delete": {name: "Подать заявку на удаление"},
            "sep2": "---------",
            "quit": {name: "Закрыть"}
        }
    });

    //контектное меню для группы
    $.contextMenu({
        selector: '.context-menu-three',
        callback: function(key, options) {
            //var m = "clicked: " + key;
            //window.console && console.log(m) || alert(m);
            switch (key){
                case "add" :
                    //addGroup($(this).text());
                    break;
            };
        },
        items: {
            "edit": {name: "Редактировать"},
            "move": {name: "Изменеть специальность"},
            "sep1": "---------",
            "delete": {name: "Подать заявку на удаление"},
            "sep2": "---------",
            "quit": {name: "Закрыть"}
        }
    });

    $('.student-list').on('mouseover',function(){
        alert('T');
    });
});

var assignSteward = function(name){
    var event = {
        act: "assignSteward",
        name: name,
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
}

var takeOffSteward = function(name){
    var event = {
        act: "takeOffSteward",
        name: name,
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
}

var getStudentCard = function(name){
    location.href = "#openModal";
    var event = {
        act: "getStudentCard",
        name: name,
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
}

var moveStudentInto = function(name){
    location.href = "#openModal";
    var event = {
        act: "moveStudentInto",
        name: name,
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
};

var moveStudentIntoGroup = function(group_id,group_name){
    var student_name = document.getElementById("student_move_name");
    if(confirm("Действительно переместить студента " + student_name.innerHTML + " в группу " + group_name)) {
        var event = {
            act: "moveStudentIntoGroup",
            name: student_name.innerHTML,
            group: group_id,
            token: localStorage.getItem('modis-token')
        };
        var str = JSON.stringify(event);
        ws.send(str);
    }
};

var addGroup = function(name){
    var newGroup = prompt("Введите название группы", "123");
    if(confirm("Действительно добавить группу " + newGroup + " ?")) {
        var event = {
            act: "addNewGroup",
            name: newGroup,
            spec: name,
            token: localStorage.getItem('modis-token')
        };
        var str = JSON.stringify(event);
        ws.send(str);
    };
}

var deleteFromGroup = function(name){
    if(confirm("Удалить студента " + name + " из данной группы ?")) {
        var event = {
            act: "moveToQuarantine",
            name: name,
            token: localStorage.getItem('modis-token')
        };
        var str = JSON.stringify(event);
        ws.send(str);
    };
};

var moreForStudent = function () {
    return function (c) {
        var b = c;
        localStorage.setItem('modis-set-id',c);
        var event = {
            act: "getStudentDiscipline",
            student_id: c,
            token: localStorage.getItem('modis-token')
        };
        var str = JSON.stringify(event);
        ws.send(str);
        var a = document.getElementById("item_sd" + b);
        c == b && (a.className = "" == a.className ? "max" : "");
        c != b && (a.className = "", a = document.getElementById("item_sd" + c), a.className = "max", b = c)
    }
}();

var openStudentDiscipline = function () {
    return function (c) {
        var b = c;
        var event = {
            act: "getMoreForDisciplineMetod",
            id: localStorage.getItem('modis-set-id'),
            discipline: c,
            token: localStorage.getItem('modis-token')
        };
        var str = JSON.stringify(event);
        ws.send(str);
        var a = document.getElementById("item"+localStorage.getItem('modis-set-id')+"_" + b);
        c == b && (a.className = "" == a.className ? "max" : "");
        c != b && (a.className = "", a = document.getElementById("item"+localStorage.getItem('modis-set-id')+"_" + c),
            a.className = "max", b = c)
    }
}();

var find = function(findString){
    if (findString.length > 1)
    {
        var event = {
            act: "find",
            findString: findString,
            token: localStorage.getItem('modis-token')
        };
        var str = JSON.stringify(event);
        ws.send(str);
    }
    else{
        var findDiv = document.getElementById("findSet");
        findDiv.innerHTML = '';
    }
};
// Список групп специальности для перемещения студента
var openSpecialtyMove = function () {
    return function (c) {
        var b = c;
        var event = {
            act: "getMoreForSpecialtyMove",
            login: localStorage.getItem('modis-login'),
            specialty: c,
            token: localStorage.getItem('modis-token')
        };
        var str = JSON.stringify(event);
        ws.send(str);
        var a = document.getElementById("item_move" + b);
        c == b && (a.className = "" == a.className ? "max" : "");
        c != b && (a.className = "", a = document.getElementById("item_move" + c), a.className = "max", b = c)
    }
}();

var addStudentMeta = function(){
    var meta_count = document.getElementById("meta_value_count");
    var card_value = document.getElementById("card_value");

    var elem_div = document.createElement('div');
    elem_div.setAttribute("id","meta_div"+meta_count.innerHTML);

    var elem_key = document.createElement('input');
    elem_key.setAttribute('type', 'text');
    elem_key.setAttribute('id', 'meta_key'+meta_count.innerHTML);
    elem_key.setAttribute('placeholder', 'Название поля');

    var elem_value = document.createElement('input');
    elem_value.setAttribute('type', 'text');
    elem_value.setAttribute('placeholder', 'Значение поля');
    elem_value.setAttribute('id', 'meta_value'+meta_count.innerHTML);
    card_value.appendChild(elem_div);
    elem_div = document.getElementById("meta_div"+meta_count.innerHTML);

    var save_button = document.createElement('input');
    save_button.setAttribute('type', 'button');
    save_button.setAttribute('onclick','saveMeta('+meta_count.innerHTML+')');
    save_button.setAttribute('value', 'Сохранить');

    elem_div.appendChild(elem_key);
    elem_div.appendChild(
        document.createTextNode(' ')
    );
    elem_div.appendChild(elem_value);
    elem_div.appendChild(
        document.createTextNode(' ')
    );
    elem_div.appendChild(save_button);
    meta_count.innerHTML++;
};

var saveMeta = function(id){
    var key = document.getElementById("meta_key"+id);
    var value = document.getElementById("meta_value"+id);
    var name = document.getElementById("card_student_name");
    var event = {
        act: "saveStudentMeta",
        name: name.innerHTML,
        key: key.value,
        value: value.value,
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
};

function changeResultM(id_ch){
    var result = document.getElementById(id_ch);
    var itog;
    if (result.checked) {itog = 1;}
    else {itog = 0}
    var event = {
        act: "changeAttendanceForStudent",
        attendance_id: result.id,
        result : itog,
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
};

