var getDiscipCP = function () {
    return function (c,semestr) {
        localStorage.setItem('modis-set-discipline',c);
        localStorage.removeItem('modis-set-cp-weight');
        localStorage.removeItem('modis-set-cp-date');
        localStorage.removeItem('modis-set-cp');
        localStorage.removeItem('modis-open-div-id');
        localStorage.removeItem('modis-set-cp-description');
        var b = c;
        var event = {
            act: "getDiscipCP",
            id: localStorage.getItem('modis-id'),
            discipline: c,
            semestr : semestr,
            token: localStorage.getItem('modis-token')
        };
        var str = JSON.stringify(event);
        ws.send(str);
        var a = document.getElementById("item_s" + b + "_" + semestr);
        c == b && (a.className = "" == a.className ? "max" : "");
        c != b && (a.className = "", a = document.getElementById("item_s" + c + "_" + semestr), a.className = "max", b = c)
    }
}();

getGroupForCP = function () {
    return function (c,w,d,des) {
        var b = c;

        var group = document.getElementById("group_list");
        group.innerHTML = '';
        localStorage.setItem('modis-set-cp',c);
        localStorage.setItem('modis-set-cp-weight',w);
        localStorage.setItem('modis-set-cp-date',d);
        localStorage.setItem('modis-set-cp-description',des);
        var event = {
            act: "getGroupForCP",
            id: c,
            token: localStorage.getItem('modis-token')
        };
        var str = JSON.stringify(event);
        ws.send(str);
        var a = document.getElementById("item" + b);

        // Закрываем открытие див, если он есть.
        var div_id = localStorage.getItem('modis-open-div-id');
        if(div_id && div_id!= "item" + c) {
            var open_div = document.getElementById(div_id);
            open_div.className = "";
            localStorage.setItem('modis-open-div-id',"item" + c);
        }
        else{
            localStorage.setItem('modis-open-div-id',"item" + b);
        }

        c == b && (a.className = "" == a.className ? "max" : "");
        c != b && (a.className = "", a = document.getElementById("item" + c), a.className = "max", b = c)
    }
}();

var listStudentsInGroupPrepod = function(group){
    var event = {
        act: "listStudentsInGroupPrepod",
        group: group,
        id: localStorage.getItem('modis-set-cp'),
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
};


$(function(){
    //контектное меню для преподователя
    $.contextMenu({
        selector: '.context-menu-four',
        callback: function(key, options) {
            //var m = "clicked: " + key;
            //window.console && console.log(m) || alert(m);
            switch (key){
                case "add" :
                        addCP($(this).text());
                    break;
            };
        },
        items: {
            "add": {name: "Добавить контрольную точку"},
            "sep5": "---------",
            "quit": {name: "Закрыть"}
        }
    });
    //контектное меню кт
    $.contextMenu({
        selector: '.context-menu-five',
        callback: function(key) {
            switch (key){
                case "add" :
                    openEditCP();
                    break;
            };
        },
        items: {
            "add": {name: "Редактировать"},
            "sep5": "---------",
            "quit": {name: "Закрыть"}
        }
    });
    //контектное меню кт
    $.contextMenu({
        selector: '.context-menu-six',
        callback: function(key) {
            switch (key){
                case "currentAttestation" :
                    generateCurrentAttestation($(this).text());
                    break;
                case "editAttPr" :
                    prepodEditAtt($(this).text());
                    break;
            };
        },
        items: {
            "currentAttestation": {name: "Сформировать текущую аттестацию"},
            "sep7": "---------",
            "editAttPr": {name: "Редактировать посещаемость"},
            "sep5": "---------",
            "quit": {name: "Закрыть"}
        }
    });
});

var addCP = function(discip){
    var event = {
        act: "addCP",
        discipline: discip,
        login: localStorage.getItem('modis-login'),
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
};

var openEditCP = function(){
    var event = {
        act: "openEditCP",
        id: localStorage.getItem('modis-set-cp'),
        weight: localStorage.getItem('modis-set-cp-weight'),
        date: localStorage.getItem('modis-set-cp-date'),
        token: localStorage.getItem('modis-token'),
        description: localStorage.getItem('modis-set-cp-description')
    };
    var str = JSON.stringify(event);
    ws.send(str);
};

function checkValue(input,max) {
    input.value = input.value.replace(/[^\d,]/g, '');
    if(input.value > max){
        input.value = 0;
        var notification = new Notification('МОДИС', {
            lang: 'ru-RU',
            body: 'Превышен лимит по количеству баллов!',
            icon: 'http://37.75.251.12:3000/images/icon.png'
        });
        setTimeout(notification.close.bind(notification), 2000);
    }
};

function checkValueResult(input) {
    input.value = input.value.replace(/[^\d,]/g, '');
    if(input.value > localStorage.getItem('modis-set-cp-weight')){
        input.value = 0;
        var notification = new Notification('МОДИС', {
            lang: 'ru-RU',
            body: 'Нельзя выставить больше веса контрольной точки!!!',
            icon: 'http://37.75.251.12:3000/images/icon.png'
        });
        setTimeout(notification.close.bind(notification), 2000);
    }
};

var add_new_cp = function() {
    var date = document.getElementById("date_modis_add_cp");
    var semester = document.getElementById("semester_kp");
    var weight = document.getElementById("weight");
    var des = document.getElementById("description-kp");
    if(date.value.length > 0 && weight.value.length >0){
        var event = {
            act: "add_new_cp",
            date: changeDate(date.value),
            weight: weight.value,
            semester: semester.innerHTML,
            discip: localStorage.getItem('modis-set-discipline'),
            description : des.value,
            token: localStorage.getItem('modis-token')
        };
        var str = JSON.stringify(event);
        ws.send(str);
        location.href = "#close";
    }
    else{
        var notification = new Notification('МОДИС', {
            lang: 'ru-RU',
            body: 'Пожалуйста заполните все поля!',
            icon: 'http://37.75.251.12:3000/images/icon.png'
        });
        setTimeout(notification.close.bind(notification), 2000);
    }
};

var edit_cp = function(cp_id){
    var event = {
        act: "editCP",
        date: changeDate(document.getElementById("date_modis").value),
        weight: document.getElementById("weight").value,
        id: cp_id,
        description: document.getElementById("description-kp").value,
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
    location.href = "#close";
};

var saveCpValue = function(result_id){
    var value = document.getElementById("value_" + result_id);
    var event = {
        act: "changeProgressValue",
        result: result_id,
        value: value.value,
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
};

var changeDate = function(inDate){
    var outDate;
    var temp = inDate.split('.');
    outDate = temp[2] + '.' + temp[1] + '.' + temp[0];
    return outDate;
};

var generateCurrentAttestation = function(group){
    var event = {
        act: "generateCurrentAttestation",
        group: group,
        discipline: localStorage.getItem('modis-set-discipline'),
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
};

var prepodEditAtt = function(group){
    var event = {
        act: "prepodEditAtt",
        group: group,
        discipline: localStorage.getItem('modis-set-discipline'),
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
};

var sendLessonPr = function () {
    return function (c) {
        var b = c;
        var event = {
            act: "getAttendanceForLessonPr",
            lesson_id: c,
            token: localStorage.getItem('modis-token')
        };
        var str = JSON.stringify(event);
        ws.send(str);
        var a = document.getElementById("item_sd" + b);
        c == b && (a.className = "" == a.className ? "max" : "");
        c != b && (a.className = "", a = document.getElementById("item_sd" + c), a.className = "max", b = c)
    }
}();

function changeResultPr(result){
    var itog;
    if (result.checked) {itog = 1;}
    else {itog = 0};
    var event = {
        act: "changeAttendanceForStudent",
        attendance_id: result.id,
        result : itog,
        token: localStorage.getItem('modis-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
};
