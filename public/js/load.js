/**
 * Created by kotainer on 04.07.2015.
 */
if(typeof(WebSocket)=="undefined") {
    alert("Your browser does not support WebSockets. Try to use Chrome or Safari.");
} else {
    var content_div = document.getElementById("content");
    var token = localStorage.getItem('modis-token');
    var ws = new WebSocket("ws://localhost:8987");
    //var ws = new WebSocket("ws://37.75.251.12:8987");
    ws.onopen = function() {
        if(token == null){
            var event = {
                act: "getLoginPage"
            };
            var str = JSON.stringify(event);
            ws.send(str);
        }
        else{
            var event = {
                act: "login-token",
                login: localStorage.getItem('modis-login'),
                token: localStorage.getItem('modis-token')
            };
            var str = JSON.stringify(event);
            ws.send(str);
        };
    };
    ws.onmessage = function(event) {
        var msg = JSON.parse(event.data);
        if (msg.status == "OK"){

            if(msg.token) { localStorage.setItem('modis-token', msg.token);}
            if(msg.login) { localStorage.setItem('modis-login', msg.login);}
            if(msg.group) { localStorage.setItem('modis-group', msg.group);}
            if(msg.id) { localStorage.setItem('modis-id', msg.id);}

            content_div.innerHTML = msg.data;
            if(msg.script) {loadScript(msg.script)};
            if(msg.style) {loadStyle(msg.style)};
        }
        if (msg.status == 'modis-content'){
            var modis_content = document.getElementById("modis-content");
            modis_content.innerHTML = msg.data;
            if(msg.script) {loadScript(msg.script)};
            if(msg.style) {loadStyle(msg.style)};
        }
        if (msg.status == 'moreForDiscipline'){
            var out = document.getElementById("item" + msg.item);
            out.innerHTML = msg.data;
        }
        if (msg.status == 'moreForDisciplineMetod'){
            var out = document.getElementById("item"+localStorage.getItem('modis-set-id')+ "_" + msg.item);
            out.innerHTML = msg.data;
        }
        if (msg.status == 'moreForSpecialty'){
            var out = document.getElementById("item_s" + msg.item);
            out.innerHTML = msg.data;
            var group = document.getElementById("group_list");
            group.innerHTML = '';
        }
        if (msg.status == 'moreForDiscipCP'){
            var out = document.getElementById("item_s" + msg.item + "_" + msg.sem);
            out.innerHTML = msg.data;
            var group = document.getElementById("group_list");
            group.innerHTML = '';
        }
        if (msg.status == 'moreForSpecialtyMove'){
            var out = document.getElementById("item_move" + msg.item);
            out.innerHTML = msg.data;
            var group = document.getElementById("group_list_move");
            group.innerHTML = '';
        }
        if (msg.status == 'moreForStudent'){
            var out = document.getElementById("item_sd" + msg.item);
            out.innerHTML = msg.data;
        }
        if(msg.status == 'listStudentsInGroup'){
            var out = document.getElementById("group_list");
            out.innerHTML = msg.data;
        }
        if(msg.status == 'openModal'){
            var out = document.getElementById("openModalContent");
            out.innerHTML = msg.data;
            location.href = "#openModal";
        }
        if(msg.status == 'closeModal'){
            location.href = "#close";
        }
        if(msg.status == 'findSet'){
            var out = document.getElementById("findSet");
            out.innerHTML = msg.findResult;
        }
        if(msg.status == 'openNewWindow'){
            var newWin = window.open("about:blank", "hello", "width=1000,height=800");
            newWin.document.write(msg.data);
        }
        if (msg.status == "notification"){
            // Давайте проверим, поддерживает ли браузер уведомления
            if (!("Notification" in window)) {
                alert("Ваш браузер не поддерживает HTML5 Notifications");
            }
            // Теперь давайте проверим есть ли у нас разрешение для отображения уведомления
            else if (Notification.permission === "granted") {
                // Если все в порядке, то создадим уведомление
                var notification = new Notification('МОДИС', {
                    lang: 'ru-RU',
                    body: msg.data,
                    icon: 'http://37.75.251.12:3000/images/icon.png'
                });
                setTimeout(notification.close.bind(notification), 2000);
            }
            // В противном случае, мы должны спросить у пользователя разрешение
            else if (Notification.permission === 'default') {
                Notification.requestPermission(function (permission) {
                    // Не зависимо от ответа, сохраняем его в настройках
                    if(!('permission' in Notification)) {
                        Notification.permission = permission;
                    }
                    // Если разрешение получено, то создадим уведомление
                    if (permission === "granted") {
                        var notification = new Notification('МОДИС', {
                            lang: 'ru-RU',
                            body: msg.data,
                            icon: 'http://37.75.251.12:3000/images/icon.png'
                        });
                        setTimeout(notification.close.bind(notification), 2000);
                    }
                    else alert(msg.data);
                });
            }
        }
    };
};
function loadStyle(style_text){
    var old_style = document.getElementById('modis-style');
    if (old_style) {
        old_style.parentNode.removeChild(old_style);
    }
    var style = document.createElement('style');
    style.setAttribute('type', 'text/css');
    style.appendChild(
        document.createTextNode(style_text)
    );
    style.id = 'modis-style';
    document.getElementsByTagName('head')[0].appendChild(style);
    //Конец загрузки стилей
};
function loadScript(script_text){
    //Загрузка cкриптов
    var id_sript = 'modis-script';
    var old_s = document.getElementById(id_sript);
    if (old_s)
    {
        old_s.parentNode.removeChild(old_s);
    }
    var s = document.createElement('script');
    s.setAttribute('type', 'text/javascript');
    s.text = script_text;
    s.id = id_sript;
    document.getElementsByTagName('head')[0].appendChild(s);
    //Конец загрузки скриптов
}