/**
 * Created by kotainer on 04.07.2015.
 */
var b_save = document.getElementById("b_save");
var email = document.getElementById("email");
b_save.onclick = function() {
    var event = {
        act: "updateUserEmail",
        login: localStorage.getItem('brs-login'),
        email: email.value,
        token: localStorage.getItem('brs-token')
    };
    var str = JSON.stringify(event);
    ws.send(str);
}
