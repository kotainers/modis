/**
 * Created by kotainer on 04.07.2015.
 */
if(typeof(WebSocket)=="undefined") {
    alert("Your browser does not support WebSockets. Try to use Chrome or Safari.");
} else {
    var content_div = document.getElementById("content");
    var token = localStorage.getItem('brs-token');
    var ws = new WebSocket("ws://localhost:8987");
    ws.onopen = function() {
        if(token == null){
            var event = {
                act: "getLoginPage"
            };
            var str = JSON.stringify(event);
            ws.send(str);
        }
        else{
            var event = {
                act: "login-token",
                login: localStorage.getItem('brs-login'),
                token: localStorage.getItem('brs-token')
            };
            var str = JSON.stringify(event);
            ws.send(str);
        };
    };
    ws.onmessage = function(event) {
        var msg = JSON.parse(event.data);
        if (msg.status == "OK"){
            content_div.innerHTML = msg.data;
            //Загрузка cкриптов
            var old_s = document.getElementById('brs-script');
            if (old_s)
            {
                old_s.parentNode.removeChild(old_s);
            }
            var s = document.createElement('script');
            s.setAttribute('type', 'text/javascript');
            s.text = msg.script;
            s.id = 'brs-script';
            document.getElementsByTagName('head')[0].appendChild(s);
            //Конец загрузки скриптов
        }
        if (msg.status == "auth_OK"){
            localStorage.setItem('brs-token', msg.token);
            localStorage.setItem('brs-login', msg.login);
            content_div.innerHTML = msg.data;
            //Загрузка cкриптов
            if(msg.script) {
                var old_s = document.getElementById('brs-script');
                if (old_s) {
                    old_s.parentNode.removeChild(old_s);
                }
                var s = document.createElement('script');
                s.setAttribute('type', 'text/javascript');
                s.text = msg.script;
                s.id = 'brs-script';
                document.getElementsByTagName('head')[0].appendChild(s);
            }
            //Конец загрузки скриптов
        }
        if (msg.status == "notification"){
            // Давайте проверим, поддерживает ли браузер уведомления
            if (!("Notification" in window)) {
                alert("Ваш браузер не поддерживает HTML5 Notifications");
            }
            // Теперь давайте проверим есть ли у нас разрешение для отображения уведомления
            else if (Notification.permission === "granted") {
                // Если все в порядке, то создадим уведомление
                var notification = new Notification('МОДИС', {
                    lang: 'ru-RU',
                    body: msg.data,
                    icon: 'http://cs608731.vk.me/v608731641/c3ed/gVtBzf6vmAU.jpg'
                });
            }
            // В противном случае, мы должны спросить у пользователя разрешение
            else if (Notification.permission === 'default') {
                Notification.requestPermission(function (permission) {
                    // Не зависимо от ответа, сохраняем его в настройках
                    if(!('permission' in Notification)) {
                        Notification.permission = permission;
                    }
                    // Если разрешение получено, то создадим уведомление
                    if (permission === "granted") {
                        var notification = new Notification('МОДИС', {
                            lang: 'ru-RU',
                            body: msg.data,
                            icon: 'http://cs608731.vk.me/v608731641/c3ed/gVtBzf6vmAU.jpg'
                        });
                    }
                    else alert(msg.data);
                });
            }
        }
    };
};
