/**
 * Created by kotainer on 07.07.2015.
 */
var b_exit = document.getElementById("b_exit");
b_exit.onclick = function() {
    var event = {
        act: "logOut",
        login: localStorage.getItem('brs-login')
    };
    var str = JSON.stringify(event);
    ws.send(str);
    localStorage.removeItem('brs-token');
    localStorage.removeItem('brs-login');
}
