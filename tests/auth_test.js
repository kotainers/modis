/**
 * Created by kotainer on 13.11.2015.
 * Тестирование авторизации и регистрации
 */
var auth = require('lib/auth');
var User = {
    status : null,
    send : function(input){
        this.status = input;
    }
}
var async = require('async')
check = {
    TestAuth : function (callback) {
        var results={
            "prepod":"process",
            "metod":"process",
            "steward":"process",
            "student":"process",
            "incorectPassword":"process",
            "notAUser":"process"
        };
        try{
            auth.Check(User,'p','p',__approot,function(err,resultP){
                auth.Check(User,'m','m',__approot,function(err,resultM){
                    auth.Check(User,'Ловчиков','qwert',__approot,function(err,resultSt){
                        auth.Check(User,'t','p',__approot,function(err,resultStud){
                            auth.Check(User,'m','p',__approot,function(err,resultInc){
                                auth.Check(User,'qwert2','qwert2',__approot,function(err,resultNot){
                                    results.prepod = (resultP=="Построили страницу")?'OK':'ERR';
                                    results.metod = (resultM=="Построили страницу")?'OK':'ERR';
                                    results.steward = (resultSt=="Построили страницу")?'OK':'ERR';
                                    results.student = (resultStud=="Построили страницу")?'OK':'ERR';
                                    results.incorectPassword = (resultInc=="Неправильный пароль")?'OK':'ERR';
                                    results.notAUser = (resultNot=="Нет пользователя")?'OK':'ERR';
                                    callback(null,results);
                                });
                            });
                        });
                    });
                });
            });
        } catch(e) {
            console.log('Ошибка при тесте авторизации ' + e);
        }
    }
};

var test = function(){
    try{
        console.time('Тестирование')
        async.parallel(check, function (err, results) {
            console.timeEnd('Тестирование');
            console.log(results);
        });
    } catch(e) {
        console.log('Ошибка при выполнении тестов ' + e);
    }
};

test();