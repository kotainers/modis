var https = require('https');
var mysql = require('mysql'),
    mysqlUtilities = require('mysql-utilities');

var connection = mysql.createConnection({
    "host":     "localhost",
    "user":     "root",
    "password": "707",
    "database": "ball"
});
connection.connect();
connection.query("SET NAMES utf8");
mysqlUtilities.upgrade(connection);
mysqlUtilities.introspection(connection);


var getRequest = function(patch,other_data,callback) {
    var options = {
        hostname: 'univeris.susu.ru',
        path: '/services/mobile/' + patch,
        headers: {'Accept': 'application/json'},
        rejectUnauthorized: false,
        method: 'GET'
    };
    var req = https.request(options, function (res) {
        var data = '';
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            data = data + chunk;
        });
        res.on('end', function () {
            return callback(null,JSON.parse(data),other_data);
        })
    });
    req.end();

    req.on('error', function (e) {
        console.error(e);
    });
};

//var p = 'GetStudySchedule/groupid/99293e6b-7edc-491d-8e6f-512226f17eb1'; // Расписание для 312 группы
var updateShedule = function(){
    var group;
    var sqlstr = "SELECT * FROM bv_group WHERE current_semester > 1";
    connection.query(sqlstr, function (err, gr) {
        if (!err) {
            group = gr;
            for(var ik = 0; ik < group.length; ik++) {
                (function (ik) {
                    setTimeout(function () {
                        loadScheduleForGroup(group[ik].univeris_id, group[ik].group_id, group[ik].current_semester);
                    }, 125000 / ik);
                })(ik);
            }
        }

    });
};

var updateGroupUniverisID = function() {
//Запрос списка групп
    var p = 'GetGroups/facultyid/d07d036e-5c02-0133-1c6d-1336c2a68eae';
    getRequest(p,'', function (err, data) {
        if (!err) {
            console.log(data);
            for (var i = 0; i < data.length; i++) {
                connection.query("UPDATE bv_group SET univeris_id = '" + data[i].Id + "' WHERE group_name = '" + data[i].GroupNumber + "'", function (err) {
                    if (err) {
                        console.log(err);
                    }
                });
            }
            console.log('Выполнено!');
        }
    });
}
var loadScheduleForGroup = function(group_univeris,group_ids, curr_semestr) {
    console.log('Загрузка для группы - ' + group_ids);
    //var group = '' + schedul[2].GroupNumber;
    //console.log(group[0]);
    // Загрузка расписания
    var p = 'GetStudySchedule/groupid/' + group_univeris;
    getRequest(p,group_ids, function (err,schedul,group_id) {
        var dubl = 0;
        for(var j = 0; j < schedul.length; j++ ){
            for(var k = 0; k < schedul.length; k++ ){
                if(j != k && schedul[j].LastName != 'Дубликат!'){
                    if(schedul[j].PairNumber == schedul[k].PairNumber &&
                        schedul[j].WeekNumber == schedul[k].WeekNumber &&
                        schedul[j].WeekDay == schedul[k].WeekDay &&
                        schedul[j].Subject == schedul[k].Subject){
                            schedul[k].LastName = 'Дубликат!';
                            dubl ++;
                    }
                }
            };
        };
        console.log(dubl);
        //console.log(schedul[2]);
        if (!err) {
            connection.query("UPDATE bv_schedule SET actual = 0 WHERE group_id ='" + group_id + "'", function () {});
            console.log(schedul.length);
            for(var i = 0; i < schedul.length; i++) {
                (function(i) {
                    setTimeout(function(){
                        if (schedul[i].LastName.length > 1 && schedul[i].LastName != 'Дубликат!'
                            && schedul[i].TermPart == 1 && schedul[i].TermNumber == curr_semestr) {
                            var sub = '' + schedul[i].Subject;
                            //console.log(sub);
                            var name = schedul[i].LastName + ' ' + schedul[i].FirstName[0] + '.' + schedul[i].MiddleName[0] + '.';
                            //Получить ИД препода
                            console.log(name);
                            var sqlstr = "SELECT user_id FROM bv_usermeta WHERE meta_key = 'FIO' AND meta_value = ?";
                            connection.query(sqlstr, name, function (err, prepod) {
                                if (prepod[0]) {
                                    console.log('Препод - ' + prepod[0].user_id);
                                    sqlstr = "SELECT bv_discipline.discipline_id FROM bv_discipline WHERE bv_discipline.name = ?";
                                    connection.query(sqlstr, sub, function (err, discip_id) {
                                        if (!err && discip_id[0]) {
                                            console.log('_________________');
                                            console.log('Итерация - ' + i);
                                            console.log('Номер пары - ' + schedul[i].PairNumber);
                                            console.log('Term nubmer - ' + schedul[i].TermNumber);
                                            console.log('Term part - ' + schedul[i].TermPart);
                                            console.log('День  - ' + schedul[i].WeekDay);
                                            console.log('Номер недели - ' + schedul[i].WeekNumber);
                                            console.log('Дисциплина - ' + discip_id[0].discipline_id);
                                            console.log('_________________');
                                            sqlstr = "INSERT INTO bv_schedule (weekday, week_number, discipline_id, " +
                                                "lec_number, group_id, ID, description) VALUES " +
                                                "(?, ?, ?, ?, ?, ?, ?)";
                                            if(schedul[i].WeekNumber != 0){
                                                param = [schedul[i].WeekDay, schedul[i].WeekNumber, discip_id[0].discipline_id,
                                                        schedul[i].PairNumber, group_id, prepod[0].user_id, ''];
                                                connection.query(sqlstr, param, function (err) {
                                                    if(err) console.log('Ошибка вставки ' + err);
                                                });
                                            }
                                            else{
                                                param = [schedul[i].WeekDay, 1, discip_id[0].discipline_id,
                                                    schedul[i].PairNumber, group_id, prepod[0].user_id, ''];
                                                connection.query(sqlstr, param, function (err) {
                                                    if(err) console.log('Ошибка вставки ' + err);
                                                });
                                                param = [schedul[i].WeekDay, 2, discip_id[0].discipline_id,
                                                    schedul[i].PairNumber, group_id, prepod[0].user_id, ''];
                                                connection.query(sqlstr, param, function (err) {
                                                    if(err) console.log('Ошибка вставки ' + err);
                                                });
                                            }
                                        }
                                        else {
                                            console.log('Вставили дисциплину');
                                            connection.query("INSERT INTO bv_discipline (name) VALUE (?)",sub, function (err) {});
                                        }
                                    });
                                }
                                else {
                                    sqlstr = "INSERT INTO bv_users (uid, group_id,user_email) VALUES(?,?,?)";
                                    var param = ['П-' + name, '36', 'П-' + name]
                                    connection.query(sqlstr, param, function (err) {
                                        console.log(err);
                                        if (!err) {
                                            sqlstr = "SELECT ID FROM bv_users WHERE uid = ?";
                                            connection.query(sqlstr, 'П-' + name, function (err, usr) {
                                                console.log(usr[0]);
                                                sqlstr = "INSERT INTO bv_usermeta (user_id, meta_key,meta_value) VALUES(?, ?,?)";
                                                param = [usr[0].ID, 'FIO', name];
                                                connection.query(sqlstr, param, function (err) {
                                                    sqlstr = "SELECT user_id FROM bv_usermeta WHERE meta_key = 'FIO' AND meta_value = ?";
                                                    connection.query(sqlstr, name, function (err, prepod) {
                                                        sqlstr = "UPDATE bv_term_relationships SET term_taxonomy_id = 4 WHERE object_id = ?";
                                                        connection.query(sqlstr, prepod[0].user_id, function (err) {
                                                            console.log('Самый низ ' + prepod[0].user_id);
                                                        });
                                                    });
                                                });
                                            });
                                        }
                                    });
                                }
                            })
                        }
                    }, 25000 / i);
                })(i);
            }
            };
    });

};

//loadScheduleForGroup('a0622ef9-e3ba-4a9b-ae84-5237ee815670','54');
//updateGroupUniverisID();
updateShedule();