/**
 * Created by kotainer on 15.07.2015.
 */
var colors = require('colors');
var connection  = require('lib/connection').mysql
var rediska = require('lib/connection').rediska;
var fs = require('fs');
var JUST = require('just');

var getLessonByDate = function(User,msg,dir){
    sqlstr='call getLessonByDate(?,?)';
    var date = new Date(msg.date);
    try{
        connection.query(sqlstr, [date,msg.group], function (err,rows) {
            if (rows[0].length) {
                var data = JSON.stringify(rows[0]);
                var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
                just.render('attendance', {d:data}, function(error, html) {
                    var tmp = html.replace('﻿','');
                    User.send(JSON.stringify(
                        {
                            status: "modis-content",
                            data: tmp
                        }));
                });
            }
            else{
                User.send(JSON.stringify(
                    {
                        status: "modis-content",
                        data: ''
                    }));
            }
        });
    }
    catch(e) {
        console.error('getLessonByDate ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var getAttendanceForLesson = function(User,msg,dir){
    sqlstr='call get_attedane_by_lesson(?)';
    try{
        connection.query(sqlstr, msg.lesson_id, function (err,rows) {
            if (rows[0].length) {
                var data = JSON.stringify(rows[0]);
                var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
                just.render('attendance_description', {d:data}, function(error, html) {
                    var tmp = html.replace('﻿','');
                    User.send(JSON.stringify(
                        {
                            status: "modis-content",
                            data: tmp
                        }));
                });
            }
        });
    }
    catch(e) {
        console.error('getAttendanceForLesson -' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var changeAttendanceForStudent = function(User,msg){
    sqlstr='call change_user_attendance(?,?)';
    try{
        connection.query(sqlstr, [msg.attendance_id,msg.result], function (err) {
            User.send(JSON.stringify(
                {
                    status: "notification",
                    data: "Данные успешно изменены."
                }));
        });
    }
    catch(e) {
        console.error('changeAttendanceForStudent -' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };

};

module.exports.getLessonByDate = getLessonByDate;
module.exports.getAttendanceForLesson = getAttendanceForLesson;
module.exports.changeAttendanceForStudent = changeAttendanceForStudent;