/**
 * Created by kotainer on 10.11.2015.
 */
// Клиент для кеша
var rediska = require('lib/connection').rediska;
var mysql  = require('lib/connection').mysql;
var cach = require('lib/caching');

var async = require('async')
    check = {
        redis: function (callback) {
			try{
				rediska.get('login-page' , function (err, repl) {
					if(!err && repl){callback(null,'ok');}
					else {
						rediska.flushall(err,function(){
							var cache = require('lib/caching');
							cache.redisCach();
							callback(null,'err');
						});
					}
				});
			} catch(e) {
				console.log('Ошибка при проверке редиски ' + e);
			}
        },
        mysql: function(callback) {
			try{
				mysql.query('SELECT 1','1',function(err,repl){
					if(!err && repl){callback(null,'ok');}
					else {callback(null,'err');}
				})
			} catch(e) {
				console.log('Ошибка при проверке муськи ' + e);
			}
        }
    };

var getStatus = function(User){
	try{
		async.parallel(check, function (err, results) {
			if(User.readyState == 1) {
				User.send(JSON.stringify(
					{
						act: "checkStatus",
						status: results
					}));
			}
		});
	} catch(e) {
		console.log('Ошибка при проверке статуса ' + e);
	}
};
var timerId = setInterval(function() {
	var selfUser={
		item: "",
		send: function(){}
	}
	getStatus(selfUser);
}, 5000);
module.exports.getStatus = getStatus;