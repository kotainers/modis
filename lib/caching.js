var colors = require('colors');
var fs = require('fs');
var rediska = require('lib/connection').rediska;
redisCach = function() {
    var now = new Date();
    // Кешируем страницы логина и регитрации
    rediska.set('login-page',
        JSON.stringify({
            status: "OK",
            data: fs.readFileSync('./template/login.html', 'utf8'),
            script : fs.readFileSync('./public/js/login.js', 'utf8')
        }));
    rediska.set('load',
        JSON.stringify({
            status: "OK",
            script : fs.readFileSync('./public/js/load.js', 'utf8')
        }));
var script;
var style;
    var page_js = fs.readFileSync('./public/js/page.js', 'utf8');
    var calendar = fs.readFileSync('./public/js/calendar.js', 'utf8');
    var prepod = fs.readFileSync('./public/js/prepod.js', 'utf8');
    var metodist = fs.readFileSync('./public/js/metodist.js', 'utf8');
    var steward = fs.readFileSync('./public/js/steward.js', 'utf8');
    var script = page_js + calendar + prepod + metodist + steward;
    var style = fs.readFileSync('./public/css/calendar.css', 'utf8') +
        fs.readFileSync('./public/css/page.css', 'utf8') +
        fs.readFileSync('./public/css/metodist.css', 'utf8') +
        fs.readFileSync('./public/css/steward.css', 'utf8');

    rediska.set('script', script);
    rediska.set('style', style);
    console.info(now.getHours() + ':' + now.getMinutes() +  ' | Кеширование проведено успешно'.green);
}
redisCach();
module.exports.redisCach = redisCach;
