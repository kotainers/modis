/**
 * Created by kotainer on 19.07.2015.
 */
var connection  = require('lib/connection').mysql
var rediska = require('lib/connection').rediska;
var fs = require('fs');
var JUST = require('just');

// Список контрольных точек для данной дисциплины и семестра
var getDiscipCP = function(User, msg, dir){
    try{
        var sqlstr='call get_discipline_cp(?,?,?)';
        var inParams=[msg.discipline, msg.semestr ,msg.id];
        connection.query(sqlstr, inParams, function (err,row){
            var data = JSON.stringify(row[0]);
            var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
            just.render('checkpoin_list', {d:data}, function(error, html) {
                if(!error){
                    var tmp = html.replace('?','');
                    User.send(JSON.stringify(
                        {
                            status: "moreForDiscipCP",
                            item: msg.discipline,
                            sem: msg.semestr,
                            data: tmp
                        }));
                }
                else{
                    console.error('getDiscipCP -' + e);
                    User.send(JSON.stringify(
                        {
                            status: "notification",
                            data: "Что-то пошло не так :( Но мы все исправим ;)"
                        }));
                }
            });
        });
    }
    catch(e) {
        console.error('getMoreForSpecialty - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

// Список студентов в группе для преподователя
var listStudentsInGroupPrepod = function(User, msg, dir){
    var param = [msg.group, msg.id];
    var sqlstr='call list_students_in_group_pr(?,?)';
    try{
        connection.query(sqlstr, param, function (err,row){
            var data = JSON.stringify(row[0]);
            var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
            just.render('list_students_in_group_prepod', {d:data}, function(error, html) {
                var tmp = html.replace('?','');
                User.send(JSON.stringify(
                    {
                        status: "listStudentsInGroup",
                        data: tmp
                    }));
            });
        });
    }
    catch(e) {
        console.error('listStudentsInGroup - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

// Список групп, для которых добавлена контрольная точка
var getGroupForCP = function(User, msg, dir){
    var sqlstr='call list_group_for_cp(?)';
    try{
        connection.query(sqlstr, msg.id, function (err,row){
            var data = JSON.stringify(row[0]);
            var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
            just.render('group_list_prepod', {d:data}, function(error, html) {
                var tmp = html.replace('?','');
                User.send(JSON.stringify(
                    {
                        status: "moreForDiscipline",
                        item: msg.id,
                        data: tmp
                    }));
            });
        });
    }
    catch(e) {
        console.error('listStudentsInGroup - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

// Модальное окно добавления контрольной точки
var addCP = function(User, msg, dir){
    var temp = msg.discipline.split('|');
    var s = temp[1].split('-');
    var sqlstr='call calculation_of_points_balance(?,?)';
    var param = [temp[0], s[1]]
    try{
        connection.query(sqlstr, param, function (err,row) {
            var val;
            if(row[0].length > 0) {
                var current = 0;
                if (row[0][0].weight) {
                    current = row[0][0].weight;
                }
                else
                    var max = 55 - current;
                if (row[0][0].itog_id == 2) {
                    max = 75 - current;
                }
                val = {
                    'discip_name': temp[0],
                    'semester': s[1],
                    'max': max

                };
            }
            else {
                val = {
                    'discip_name': temp[0],
                    'semester': s[1],
                    'max': 60

                };
            }
            var data = JSON.stringify(val);
            var just = new JUST({root: dir + '/view', useCache: false, ext: '.html'});
            just.render('modal_addCP', {d: data}, function (error, html) {
                var tmp = html.replace('?', '');
                User.send(JSON.stringify(
                    {
                        status: "openModal",
                        data: tmp
                    }));
            });

        });
    }
    catch(e) {
        console.error('listStudentsInGroup - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

// Добавление новой контрольной точки
var addNewCP = function(User,msg){
    var sqlstr='SELECT  add_new_cp(?,?,?,?,?)';
    var param = [msg.discip, msg.semester, msg.date, msg.weight,msg.description]
    try{
        connection.query(sqlstr, param, function (err,row) {
            if(!err){
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Контрольная точно успешно добавлена."
                    }));
            }
            else{
                console.log(err);
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Что-то пошло не так :( Но мы все исправим ;)"
                    }));
            }
        });
    }
    catch(e) {
        console.error('listStudentsInGroup - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};


var changeProgressValue = function(User,msg){
    var sqlstr='call change_progress_value(?,?)';
    var param = [msg.result, msg.value]
    try{
        connection.query(sqlstr, param, function (err,row){
            if(!err){
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Данные успешно обновлены."
                    }));
            }
            else{
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Что-то пошло не так :( Но мы все исправим ;)"
                    }));
            }

        });
    }
    catch(e) {
        console.error('changeProgressValue - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };

};

var editCP = function(User,msg){
    var sqlstr='call update_checkpoint(?,?,?,?)';
    var param = [msg.id, msg.date, msg.weight,msg.description]
    try{
        connection.query(sqlstr, param, function (err){
            if(!err){
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Данные успешно обновлены."
                    }));
            }
            else{
                //throw err;
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Что-то пошло не так :( Но мы все исправим ;)"
                    }));
            }

        });
    }
    catch(e) {
        console.error('editCP - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var openEditCP = function(User,msg,dir){
    try{
        var data = JSON.stringify(msg);
        var just = new JUST({root: dir + '/view', useCache: false, ext: '.html'});
        just.render('modal_edit_cp', {d: data}, function (error, html) {
            var tmp = html.replace('?', '');
            User.send(JSON.stringify(
                {
                    status: "openModal",
                    data: tmp
                }));
        });

    }
    catch(e) {
        console.error('openEditCP - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var generateCurrentAttestation = function(User,msg,dir){
    try{
        var sqlstr='call get_current_appraisal_for_group_on_discipline(?,?)';
        var param = [msg.group, msg.discipline];
        connection.query(sqlstr, param, function (err,row){
            if(!err){
                var now = new Date();
                var data = {};
                data.balls = row[0];
                data.all_balls = row[1][0].all_balls;
                data.att = row[2];
                data.all_att = row[3][0].all;
                data.discip_name = row[4][0].name;
                data.group = msg.group;
                data.date = now.getDate() + '.' + (now.getMonth()+1) + '.' + now.getFullYear();
                data.name = User.name;
                var _data = JSON.stringify(data);
                var just = new JUST({root: dir + '/view', useCache: false, ext: '.html'});
                just.render('current_attestation', {d: _data}, function (error, html) {
                    if(!error) {
                        var tmp = html.replace('?', '');
                        User.send(JSON.stringify(
                            {
                                status: "openNewWindow",
                                data: tmp
                            }));
                    }
                    else{
                        console.log('Ошибка ' + error);
                        User.send(JSON.stringify(
                            {
                                status: "notification",
                                data: "Что-то пошло не так :( Но мы все исправим ;)"
                            }));
                    }
                });
            }
            else{
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Что-то пошло не так :( Но мы все исправим ;)"
                    }));
            }

        });

    }
    catch(e) {
        console.error('generateCurrentAttestation - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var prepodEditAtt = function(User,msg,dir){
    try{
        var sqlstr='call get_group_lesson_date(?,?,?)';
        var param = [User.id, msg.discipline,msg.group];
        connection.query(sqlstr, param, function (err,row) {
            var data = JSON.stringify(row[0]);
            var just = new JUST({root: dir + '/view', useCache: false, ext: '.html'});
            just.render('prepod_attendance_date', {d: data}, function (error, html) {
                var tmp = html.replace('?', '');
                User.send(JSON.stringify(
                    {
                        status: "listStudentsInGroup",
                        data: tmp
                    }));
            });
        });
    }
    catch(e) {
        console.error('prepodEditAtt - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var getAttendanceForLessonPr = function(User,msg,dir){
    sqlstr='call get_attedane_by_lesson(?)';
    try{
        connection.query(sqlstr, msg.lesson_id, function (err,rows) {
            if (rows[0].length) {
                var data = JSON.stringify(rows[0]);
                var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
                just.render('attendance_description_prepod', {d:data}, function(error, html) {
                    if(!error) {
                        var tmp = html.replace('﻿', '');
                        User.send(JSON.stringify(
                            {
                                status: "moreForStudent",
                                item: msg.lesson_id,
                                data: tmp
                            }));
                    }
                    else{
                        console.error('getAttendanceForLessonPr -' + e);
                        User.send(JSON.stringify(
                            {
                                status: "notification",
                                data: "Что-то пошло не так :( Но мы все исправим ;)"
                            }));
                    }
                });
            }
        });
    }
    catch(e) {
        console.error('getAttendanceForLessonPr -' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

module.exports.getDiscipCP = getDiscipCP;
module.exports.listStudentsInGroupPrepod = listStudentsInGroupPrepod;
module.exports.getGroupForCP = getGroupForCP;
module.exports.addCP = addCP;
module.exports.addNewCP = addNewCP;
module.exports.changeProgressValue = changeProgressValue;
module.exports.editCP = editCP;
module.exports.openEditCP = openEditCP;
module.exports.generateCurrentAttestation = generateCurrentAttestation;
module.exports.prepodEditAtt = prepodEditAtt;
module.exports.getAttendanceForLessonPr = getAttendanceForLessonPr;