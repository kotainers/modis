
var connection  = require('lib/connection').mysql
var colors = require('colors');
var result= {
    one: true,
    two: true,
    three: true,
    four: true,
    five : true,
	six : true,
    seven : true
};
var now = new Date();
var raitingHourUpdate = now.getHours();

Date.prototype.getWeek = function () {
    var target  = new Date(this.valueOf());
    var dayNr   = (this.getDay() + 6) % 7;
    target.setDate(target.getDate() - dayNr + 3);
    var firstThursday = target.valueOf();
    target.setMonth(0, 1);
    if (target.getDay() != 4) {
        target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
    }
    var _week = Math.ceil((firstThursday - target) / 604800000); //на одну больше
    if (_week % 2 == 0) {
        return 1;
    }
    else {
        return 2;
    }
};

var cronJobDay=require('cron').CronJob;

var jobRaiting = new cronJobDay({
    cronTime: '* 5 * * * *',
    onTick: function() {
        if (now.getHours() > raitingHourUpdate){
            raitingHourUpdate = now.getHours();
            var sqlstr='call update_full_raiting()';
            connection.query(sqlstr, function (err) {
                if(err){
                    console.log(now.getHours()
                        + ':' + now.getMinutes() + err +' Обновление рейтинга не выполнено!'.red);
                }
                else{
                    console.log(now.getHours()
                        + ':' + now.getMinutes() + ' Рейтинг успешно обновлен!'.green);
                }
            });
        };
    },
    start: true
});

var jobDay = new cronJobDay({
    //cronTime: '* 5 * * * 1-6',
    cronTime: '* * * * * 1-6',
    onTick: function() {
        now = new Date();
        var week = new Date().getWeek();

        // 1 пара
        if (now.getHours() == 7 && now.getMinutes() == 59 && result.one){result.one = !result.one;};
        if (now.getHours() >= 8 && now.getMinutes() >= 0 && !result.one){
            lessonProcess(week,1);
            result.one = !result.one;
        };
        // Конец обработки 1 пары

        // 2 пара
        if (now.getHours() == 9 && now.getMinutes() == 44 && result.two){result.two = !result.two;};
        if (now.getHours() >= 9 && now.getMinutes() >= 45 && !result.two){
            lessonProcess(week,2);
            result.two = !result.two;
        }
        // Конец обработки 2 пары

        // 3 пара
        if (now.getHours() == 11 && now.getMinutes() == 29 && result.three){result.three = !result.three;};
        if (now.getHours() >= 11 && now.getMinutes() >= 30 && !result.three){
            lessonProcess(week,3);
            result.three = !result.three;
        }
        // Конец обработки 3 пары

        // 4 пара
        if (now.getHours() == 13 && now.getMinutes() == 34 && result.four){result.four = !result.four;};
        if (now.getHours() >= 13 && now.getMinutes() >= 35 && !result.four){
            lessonProcess(week,4);
            result.four = !result.four;
        }
        // Конец обработки 4 пары

        // 5 пара
        if (now.getHours() == 15 && now.getMinutes() == 9 && result.five){result.five = !result.five;};
        if (now.getHours() >= 15 && now.getMinutes() >= 10 && !result.five){
            lessonProcess(week,5);
            result.five = !result.five;
        }
        // Конец обработки 5 пары
		
		// 6 пара
        if (now.getHours() == 17 && now.getMinutes() == 9 && result.six){result.six = !result.six;};
        if (now.getHours() >= 17 && now.getMinutes() >= 10 && !result.six){
            lessonProcess(week,6);
            result.six = !result.six;
        }
        // Конец обработки 6 пары

        // 7 пара
        if (now.getHours() == 18 && now.getMinutes() == 49 && result.seven){result.seven = !result.seven;};
        if (now.getHours() >= 18 && now.getMinutes() >= 50 && !result.six){
            lessonProcess(week,7);
            result.seven = !result.seven;
        }
        // Конец обработки 7 пары
    },
    start: false
});

var lessonProcess = function(week_number,lec_number){
    var inParams=[week_number,now.getDay(),lec_number];
    var sqlstr='call get_schedule(?,?,?)';
    // Вызов процедуры бд
    connection.query(sqlstr, inParams, function (err,rows) {
        if (!err) {
            var count = 0;
            rows[0].forEach(function(item){
                sqlstr='call new_lesson(?,?)';
                connection.query(sqlstr, [item.schedule_id,item.group_id], function (err) {
                    if (err) {console.log(now.getHours() + ':' + now.getMinutes() + ' Вставка пары не прошла!'.red)};
                });
                count++;
            });
            console.log(now.getHours() + ':' + now.getMinutes() + ' | ' + lec_number + ' пара обработана для '.cyan +
                count +' групп'.cyan +  ' | Неделя - '.green + new Date().getWeek());
        }
        else console.log(err)
    });
};

console.log(now.getHours() + ':' + now.getMinutes() +  ' | Старт задач'.green +  ' | Неделя - '.green + new Date().getWeek());
jobDay.start();
module.exports = cronJobDay;