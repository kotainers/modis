/**
 * Created by Kotainer on 10.06.15.
 */
var connection  = require('lib/connection').mysql;
var rediska = require('lib/connection').rediska;
var JUST = require('just');
var fs = require('fs');
var send_email = require('lib/send_mail');
var crypto = require('crypto');

var updateUserEmail = function(User,login,email,dir){
    var sqlstr='call update_user_email(?,?)';
    var inParams=[login, email];
    try{
    connection.query(sqlstr, inParams, function (err,row){
        var just = new JUST({ root : dir + '/view', useCache : true, ext : '.html' });
        rediska.hmget(login , 'description' , function (err, description) {
            just.render('page', {d:JSON.parse(description[0])}, function(error, html) {
                var tmp = html.replace('﻿','');
                User.send(JSON.stringify(
                    {
                        status: "auth_OK",
                        token: token,
                        login: login,
                        data: tmp
                    }));
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: 'Еmail успешно обновлен!'
                    }));
            });
        });
    });
    }
    catch(e) {
        console.error('updateUserEmail - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var getMoreForDiscipline = function(User, msg, dir){
    var sqlstr='call get_attendance_for_discipline(?,?)';
    var inParams=[msg.id, msg.discipline];
    try {
        connection.query(sqlstr, inParams, function (err, row) {
            var _data = row[0][0];
            _data.user_id = msg.id;
            _data.discipline = msg.discipline;
            sqlstr='call get_discipline_progress(?,?)';
            inParams=[msg.id, msg.discipline];
            connection.query(sqlstr, inParams, function (err, rows) {
                if(rows[0][0] == undefined){
                    _data.discip_itog = 2;
                    _data.ball = 0;
                }
                else{
                    _data.discip_itog = rows[0][0].itog;
                    _data.ball = rows[0][0].balls;
                }
                var data = JSON.stringify(_data);
                var just = new JUST({root: dir + '/view', useCache: false, ext: '.html'});
                just.render('discipline_more', {d: data}, function (error, html) {
                    var tmp = html.replace('﻿', '');
                    User.send(JSON.stringify(
                        {
                            status: "moreForDiscipline",
                            item: msg.discipline,
                            data: tmp
                        }));
                });
            });
        });
    }
    catch(e) {
            console.error('getMoreForDiscipline - ' + e);
            User.send(JSON.stringify(
                {
                    status: "notification",
                    data: "Что-то пошло не так :( Но мы все исправим ;)"
                }));
    };
};

var disciplineAttendanceDetails = function(User, msg, dir){
    var sqlstr='call disciplineAttendanceDetails(?,?)';
    var inParams=[msg.login, msg.discipline];
    try {
        connection.query(sqlstr, inParams, function (err, row) {
            var _data = row[0];
            _data[0].discipline = msg.discipline;
            var data = JSON.stringify(_data);
            var just = new JUST({root: dir + '/view', useCache: false, ext: '.html'});
            just.render('attendance_details', {d: data}, function (error, html) {
                var tmp = html.replace('﻿', '');
                User.send(JSON.stringify(
                    {
                        status: "openModal",
                        data: tmp
                    }));
            });
        });
    }
    catch(e) {
        console.error('disciplineAttendanceDetails - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var disciplineProgressDetails = function(User, msg, dir){
    var sqlstr='call disciplineProgressDetails(?,?)';
    var inParams=[msg.login, msg.discipline];
    try {
        connection.query(sqlstr, inParams, function (err, row) {
            var _data = row[0];
            var data = JSON.stringify(_data);
            var just = new JUST({root: dir + '/view', useCache: false, ext: '.html'});
            just.render('progress_details', {d: data}, function (error, html) {
                var tmp = html.replace('﻿', '');
                User.send(JSON.stringify(
                    {
                        status: "openModal",
                        data: tmp
                    }));
            });
        });
    }
    catch(e) {
        console.error('disciplineProgressDetails - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var restorePassword = function(User,msg){
    var sqlstr='call find_user_by_email(?)';
    try {
        connection.query(sqlstr, msg.email, function (err, row) {
            if(err){
                console.log("Ошибка при восстановлении пароля" + err);
            }
            else if (row[0].length > 0){
                var salt = Math.random() + ' nark';
                var password = generatePassword();
                var encPass = encryptPassword(password,salt);
                send_email.send(msg.email,"Восстановление пароля",
                    "Ваш логин - " + row[0][0].user_login + " Ваш новый пароль : " + password);
                sqlstr='select updatePassword(?,?,?)';
                var param = [row[0][0].ID, encPass, salt];
                connection.query(sqlstr, param, function (errs) {
                    if (errs) {console.log("Ошибка при восстановлении пароля" + errs);}
                    else{
                        User.send(JSON.stringify(
                            {
                                status: "notification",
                                data: "Данные отправлены на указанный адрес."
                            }));
                    }
                });
            }
            else{
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Пользователя с таким адресом не существует!"
                    }));
            }
        });
    }
    catch(e) {
        console.error('disciplineProgressDetails - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};
// Функция зашифровки пароля
encryptPassword = function(password,salt) {
    return crypto.createHmac('sha1', salt).update(password).digest('hex');
};

function makeRand(max){
    // Generating random number from 0 to max (argument)
    return Math.floor(Math.random() * max);
}

generatePassword = function(){
    var password_length = 10,
        symbols = [
            'A','B','C','D','E','F','G','H','I','J',
            'K','L','M','N','O','P','Q','R','S',
            'T','U','V','W','X','Y','Z',
            'a','b','c','d','e','f','g','h','i','j',
            'k','l','m','n','o','p','q','r','s',
            't','u','v','w','x','y','z',
            1,2,3,4,5,6,7,8,9,0
        ],result = '';
    for (i = 0; i < password_length; i++){
        result += symbols[makeRand(symbols.length)];
    }
    return result;
};

module.exports.restorePassword = restorePassword;
module.exports.updateUserEmail = updateUserEmail;
module.exports.getMoreForDiscipline = getMoreForDiscipline;
module.exports.disciplineAttendanceDetails = disciplineAttendanceDetails;
module.exports.disciplineProgressDetails = disciplineProgressDetails;
