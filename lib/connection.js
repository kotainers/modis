/**
 * Created by Kotainer on 10.06.15.
 */
//MySql
var colors = require('colors');
var config = require('config');
var mysql = require('mysql'),
    mysqlUtilities = require('mysql-utilities');
var now = new Date();

var connection = mysql.createConnection(config.get('mysql'));
connection.query("SET NAMES utf8");
mysqlUtilities.upgrade(connection);
mysqlUtilities.introspection(connection);
function replaceClientOnDisconnect(connection) {
    connection.on("error", function (err) {
        if (!err.fatal) {
            return;
        }
        if (err.code !== "PROTOCOL_CONNECTION_LOST") {
            throw err;
        }
        connection = mysql.createConnection(config.get('mysql'));
        connection.query("SET NAMES utf8");
        mysqlUtilities.upgrade(connection);
        mysqlUtilities.introspection(connection);
        replaceClientOnDisconnect(connection);
        connection.connect(function (error) {
            console.log(now.getHours() + ':' + now.getMinutes() + ' | Переподключили муську'.yellow);
            if (error) {
                console.log(now.getHours() + ':' + now.getMinutes() +  ' | Ошибка переподключения муськи'.red + error);
            }
        });
    });
}
replaceClientOnDisconnect(connection);

var timerMysql = setInterval(function() {
    connection.query('SELECT 1');
}, 10000);

var redis = new require('redis');
var rediska = new redis.createClient();
rediska.select(2);
rediska.set('check','check');
var timerRedis = setInterval(function() {
    rediska.get('check');
}, 150000);

console.log(now.getHours() + ':' + now.getMinutes() + ' | Подключили муську и редис'.green);

module.exports.mysql = connection;
module.exports.rediska = rediska;
