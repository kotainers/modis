/**
 * Created by kotainer on 19.07.2015.
 */
var connection  = require('lib/connection').mysql
var rediska = require('lib/connection').rediska;
var fs = require('fs');
var JUST = require('just');

var getMoreForSpecialty = function(User, msg,dir){
    try{
        var sqlstr='call group_list(?)';
        connection.query(sqlstr, msg.specialty, function (err,row){
            var data = JSON.stringify(row[0]);
            var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
            just.render('group_list', {d:data}, function(error, html) {
                var tmp = html.replace('?','');
                User.send(JSON.stringify(
                    {
                        status: "moreForSpecialty",
                        item: msg.specialty,
                        data: tmp
                    }));
            });
        });
    }
    catch(e) {
        console.error('getMoreForSpecialty - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var getMoreForSpecialtyMove = function(User, msg,dir){
    try{
        var sqlstr='call group_list(?)';
        connection.query(sqlstr, msg.specialty, function (err,row){
            var data = JSON.stringify(row[0]);
            var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
            just.render('group_list_move', {d:data}, function(error, html) {
                var tmp = html.replace('?','');
                User.send(JSON.stringify(
                    {
                        status: "moreForSpecialtyMove",
                        item: msg.specialty,
                        data: tmp
                    }));
            });
        });
    }
    catch(e) {
        console.error('getMoreForSpecialtyMove - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var listStudentsInGroup = function(User, msg, dir){
    var sqlstr='call list_students_in_group(?)';
    try{
        connection.query(sqlstr, msg.group, function (err,row){
            var data = JSON.stringify(row[0]);
            var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
            just.render('list_students_in_group', {d:data}, function(error, html) {
                var tmp = html.replace('?','');
                User.send(JSON.stringify(
                    {
                        status: "listStudentsInGroup",
                        data: tmp
                    }));
            });
        });
    }
    catch(e) {
        console.error('listStudentsInGroup - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var getStudentDiscipline = function(User, msg, dir){
    var sqlstr='call get_student_group(?)';
    connection.query(sqlstr, msg.student_id, function (errors,row) {
        sqlstr='call get_list_of_disciplines_for_semestr(?)';
        connection.query(sqlstr, row[0][0].group_id, function (error,row_discipline) {
            try{
                var _data = row_discipline[0];
                _data[0].user_login = row[0][0].ID;
                var data = JSON.stringify(_data);
                var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
                just.render('discipline_list', {d:data}, function(error, html) {
                    var tmp = html.replace('?','');
                    User.send(JSON.stringify(
                        {
                            status: "moreForStudent",
                            data: tmp,
                            item: msg.student_id,
                        }));
                });
            }
            catch(e) {
                console.error('getStudentDiscipline - ' + e);
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Что-то пошло не так :( Но мы все исправим ;)"
                    }));
            };
        });
    });
};

var getMoreForDisciplineMetod = function(User, msg, dir){
    try{
        var sqlstr='call get_attendance_for_discipline(?,?)';
        var inParams=[msg.id, msg.discipline];
        connection.query(sqlstr, inParams, function (err, row) {
            var _data = row[0][0];
            _data.user_id = msg.id;
            _data.discipline = msg.discipline;
            sqlstr='call get_discipline_progress(?,?)';
            inParams=[msg.id, msg.discipline];
            connection.query(sqlstr, inParams, function (err, rows) {
                if(rows[0][0] == undefined){
                    _data.discip_itog = 2;
                    _data.ball = 0;
                }
                else{
                    _data.discip_itog = rows[0][0].itog;
                    _data.ball = rows[0][0].balls;
                }
                var data = JSON.stringify(_data);
                var just = new JUST({root: dir + '/view', useCache: false, ext: '.html'});
                just.render('discipline_more_metod', {d: data}, function (error, html) {
                    var tmp = html.replace('﻿', '');
                    User.send(JSON.stringify(
                        {
                            status: "moreForDisciplineMetod",
                            item: msg.discipline,
                            data: tmp
                        }));
                });
            });
        });
    }
    catch(e) {
        console.error('getMoreForDisciplineMetod -' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var find = function(User, msg, dir){
    try{
        var findString = '%' + msg.findString;
        if(isNaN(findString))
        {
            findString = findString.toLowerCase();
            if (findString.indexOf("эип") != -1){
                findString = findString.replace(/[^\d,]/g, '');
                findGroup(User, findString, dir);
            }
            else{
                findString += '%';
                var sqlstr='call find_student(?)';
                connection.query(sqlstr, findString, function (err,row){
                    var data = JSON.stringify(row[0]);
                    var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
                    just.render('find_student_list', {d:data}, function(error, html) {
                        var tmp = html.replace('﻿','');
                        User.send(JSON.stringify(
                            {
                                status: "findSet",
                                findResult: tmp
                            }
                        ));
                    });
                });
            }
        }
        else {
            findGroup(User, findString, dir);
        }
    } catch(e) {
        console.error('find -' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var findGroup = function(User, findString,dir){
    findString += '%';
    var sqlstr='call find_group(?)';
    connection.query(sqlstr, findString, function (err,row){
        var data = JSON.stringify(row[0]);
        var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
        just.render('find_group_list', {d:data}, function(error, html) {
            var tmp = html.replace('﻿','');
            User.send(JSON.stringify(
                {
                    status: "findSet",
                    findResult: tmp
                }
            ));
        });
    });
};

var moveToQuarantine = function(User,msg){
    try{
        var sqlstr='SELECT move_to_quarantine(?)';
        // Вызов процедуры бд
        connection.query(sqlstr, msg.name, function (err,rows) {
            if (!err) {
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Студент " + msg.name +  " перемещен в карантин."
                    }));
            }
        });
    } catch(e) {
        console.error('moveToQuarantine -' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var addNewGroup = function(User,msg){
    try{
        var inParams=[msg.name, msg.spec];
        var sqlstr='SELECT add_new_group(?,?)';
        // Вызов процедуры бд
        connection.query(sqlstr, inParams, function (err,rows) {
            if (!err) {
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Группа " + msg.name +  " успешна добавлена."
                    }));
            }
            else{
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Группа " + msg.name +  " уже сущетвует. Пожалуйста введите другое название."
                    }));
            }
        });
    } catch(e) {
        console.error('moveToQuarantine -' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var moveStudentInto = function(User,msg,dir){
    try{
        sqlstr='call get_specialty_list()';
        connection.query(sqlstr, function (errors,row_specialty) {
            row_specialty[0][0].student_name = msg.name;
            var data = JSON.stringify(row_specialty[0]);
            var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
            just.render('specialty_list', {d:data}, function(error, html) {
                var tmp = html.replace('﻿','');
                User.send(JSON.stringify(
                    {
                        status: "openModal",
                        data: tmp
                    }
                ));
            });
        });
    } catch(e) {
        console.error('moveStudentInto -' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

var moveStudentIntoGroup = function(User,msg){
    try{
        var inParams=[msg.group, msg.name];
        var sqlstr='SELECT move_student_into_group(?,?)';
        // Вызов процедуры бд
        connection.query(sqlstr, inParams, function (err,rows) {
            if (!err) {
                User.send(JSON.stringify(
                    {
                        status: "closeModal"
                    }));
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Студент успешно перенесён."
                    }));
            }
            else{
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Операция невозможна или недопутима."
                    }));
            }
        });
    } catch(e) {
        console.error('moveStudentIntoGroup -' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };

};

var getStudentCard = function(User, msg, dir){
    try{
        sqlstr='call get_student_info(?)';
        connection.query(sqlstr,msg.name, function (errors,row) {
            row[0][0].student_name = msg.name;
            var data = JSON.stringify(row[0]);
            var just = new JUST({ root : dir + '/view', useCache : false, ext : '.html' });
            just.render('student_card', {d:data}, function(error, html) {
                var tmp = html.replace('﻿','');
                User.send(JSON.stringify(
                    {
                        status: "openModal",
                        data: tmp
                    }
                ));
            });
        });

    } catch(e) {
        console.error('getStudentCard -' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
}

var saveStudentMeta = function(User,msg){
    if(msg.key.length >0 && msg.value.length >0) {
        try {
            var inParams = [msg.name, msg.key, msg.value];
            var sqlstr = 'SELECT save_student_meta(?,?,?)';
            // Вызов процедуры бд
            connection.query(sqlstr, inParams, function (err, rows) {
                if (!err) {
                    User.send(JSON.stringify(
                        {
                            status: "notification",
                            data: "Данные успешно сохранены."
                        }));
                }
                else {
                    User.send(JSON.stringify(
                        {
                            status: "notification",
                            data: "Операция невозможна или недопутима."
                        }));
                }
            });
        } catch (e) {
            console.error('moveStudentIntoGroup -' + e);
            User.send(JSON.stringify(
                {
                    status: "notification",
                    data: "Что-то пошло не так :( Но мы все исправим ;)"
                }));
        }
        ;
    }
    else{
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Возможно не стоит сохранять пустые поля ;)"
            }));
    }
};
var takeOffSteward = function(User, msg){
    try{
        var sqlstr='SELECT take_off_steward(?)';
        // Вызов процедуры бд
        connection.query(sqlstr, msg.name, function (err) {
            if (!err) {
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Студент " + msg.name + " снят с должности старосты."
                    }));
            }
            else{
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Операция невозможна или недопутима."
                    }));
            }
        });
    } catch(e) {
        console.error('takeOffSteward -' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };

};
var assignSteward = function(User, msg){
    try{
        var sqlstr='SELECT assign_steward(?)';
        // Вызов процедуры бд
        connection.query(sqlstr, msg.name, function (err,rows) {
            if (!err) {
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Студент " + msg.name + " успешно назначен старостой."
                    }));
            }
            else{
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Операция невозможна или недопутима."
                    }));
            }
        });
    } catch(e) {
        console.error('assignSteward -' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };

};

var disciplineAttendanceDetailsMetod = function(User, msg, dir){
    var sqlstr='call disciplineAttendanceDetails(?,?)';
    var inParams=[msg.login, msg.discipline];
    try {
        connection.query(sqlstr, inParams, function (err, row) {
            var _data = row[0];
            _data[0].discipline = msg.discipline;
            var data = JSON.stringify(_data);
            var just = new JUST({root: dir + '/view', useCache: false, ext: '.html'});
            just.render('attendance_details_metod', {d: data}, function (error, html) {
                var tmp = html.replace('﻿', '');
                User.send(JSON.stringify(
                    {
                        status: "openModal",
                        data: tmp
                    }));
            });
        });
    }
    catch(e) {
        console.error('disciplineAttendanceDetailsMetod - ' + e);
        User.send(JSON.stringify(
            {
                status: "notification",
                data: "Что-то пошло не так :( Но мы все исправим ;)"
            }));
    };
};

module.exports.getMoreForSpecialty = getMoreForSpecialty;
module.exports.getMoreForSpecialtyMove = getMoreForSpecialtyMove;
module.exports.listStudentsInGroup = listStudentsInGroup;
module.exports.getStudentDiscipline = getStudentDiscipline;
module.exports.getMoreForDisciplineMetod = getMoreForDisciplineMetod;
module.exports.find = find;
module.exports.moveToQuarantine = moveToQuarantine;
module.exports.addNewGroup = addNewGroup;
module.exports.moveStudentInto = moveStudentInto;
module.exports.moveStudentIntoGroup = moveStudentIntoGroup;
module.exports.getStudentCard = getStudentCard;
module.exports.saveStudentMeta = saveStudentMeta;
module.exports.assignSteward = assignSteward;
module.exports.disciplineAttendanceDetailsMetod = disciplineAttendanceDetailsMetod;
module.exports.takeOffSteward  = takeOffSteward ;
