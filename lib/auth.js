var crypto = require('crypto');
var config = require('config');
var JUST = require('just');
var colors = require('colors');
var connection  = require('lib/connection').mysql
var rediska = require('lib/connection').rediska;
var fs = require('fs');
var now = new Date();
Date.prototype.getWeek = function () {
    var target  = new Date(this.valueOf());
    var dayNr   = (this.getDay() + 6) % 7;
    target.setDate(target.getDate() - dayNr + 3);
    var firstThursday = target.valueOf();
    target.setMonth(0, 1);
    if (target.getDay() != 4) {
        target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
    }
    var _week = 1 + Math.ceil((firstThursday - target) / 604800000); //на одну больше
    if (_week % 2 == 0) {
        return 1;
    }
    else {
        return 2;
    }
};

// Авторизация пользователя
var Check = function (User,login,password,dir,callback) {
    now = new Date();
    var cryptLogin = encryptPassword(login,'nark');
    // Если нету токена, то выбираем пользователя из базы
    var sqlstr='call auth(?)';
    connection.query(sqlstr, login, function (err,row){
        try{
            var user = row[0][0];
            if (user.salt) {
                // Проверяем пароль
                if (user.user_pass == encryptPassword(password,user.salt) ) {
                    // Создаем новый токен
                    var token = crypto.createHmac('sha1', user.salt + config.get('secret')).update(password).digest('hex');
                    User.token = token;
                    // Запиываем токен в базу и устанавливаем срок жизни
                    rediska.hdel(cryptLogin , 'token');
                    rediska.hset(cryptLogin, 'token' ,token);
                    rediska.expire(cryptLogin,10800);
                    // Получаем полную информацию по пользователю
                    getFullUserDescriptions(login,function(err,descip){
                        if(!err){
                            var description = descip;
                            User.id = description.ID;
                            // Для студентов делаем доп выборку данных по дисциплинам,
                            // посещаемости и успеваемости
                            if(description.term_group < 4){
                                sqlstr='call get_list_of_disciplines_for_semestr(?)';
                                connection.query(sqlstr, description.group_id, function (errors,row_discipline){
                                    description.disipline = row_discipline[0];
                                    getScheduleForGroup(description.group_id, function(err,schedule){
                                        if(!err) {
                                            description.schedule = schedule;
                                            getBest50(function (err, row_best) {
                                                if(!err) {
                                                    description.best = row_best;
                                                    getBestOfGroup(description.group_id, function (err, best_group) {
                                                        description.best_group = best_group;
                                                        rediska.hset(cryptLogin, 'description', JSON.stringify(description));
                                                        //console.log('Строим страницу студента после авторизации');
                                                        render(User, dir, description, token, login, function(err,status){callback(err,status)});
                                                    })
                                                }
                                            });
                                        }
                                    })
                                });
                            }
                            if(description.term_group == 4) {
                                User.name = description.fio;
                                //console.log('Строим страницу преподователя после авторизации');
                                sqlstr = 'call get_teach_disipline(?)';
                                connection.query(sqlstr, description.ID,function (errors,row_discip) {
                                    description.discip = row_discip[0];
                                    sqlstr = 'call get_teach_schedule(?,?)';
                                    var param = [description.ID,new Date().getWeek()]
                                    connection.query(sqlstr, param,function (errs,row_schedule) {
                                        if (errs) throw errs;
                                        description.schedule = row_schedule[0];
                                        rediska.hset(cryptLogin, 'description', JSON.stringify(description));
                                        render(User, dir, description, token, login, function(err,status){callback(err,status)});
                                    });
                                });
                            }
                            if(description.term_group == 5){
                                //console.log(now.getHours() + ':' + now.getMinutes() + ' Строим страницу методиста после авторизации');
                                sqlstr='call get_specialty_list()';
                                connection.query(sqlstr, function (errors,row_specialty) {
                                    description.specialty = row_specialty[0];
                                    sqlstr='call group_att_raiting()';
                                    connection.query(sqlstr, function (errors,row_group_raiting) {
                                        var group_raiting = [];
                                        for(gr = 0; gr < row_group_raiting[1].length-1; gr++){
                                            group_raiting[gr] = {
                                                'group_name':row_group_raiting[0][gr].group_name,
                                                'all':row_group_raiting[0][gr].all,
                                                'att':row_group_raiting[1][gr].att,
                                                'percent': Math.round(
                                                    row_group_raiting[1][gr].att/row_group_raiting[0][gr].all*100)
                                            };
                                        }
                                        group_raiting = group_raiting.sort(function(obj1, obj2) {
                                            return obj2.percent-obj1.percent;
                                        });
                                        description.group_raiting = group_raiting;
                                        // Записываем данные в кеш
                                        rediska.hset(cryptLogin, 'description' ,JSON.stringify(description));
                                        render(User, dir, description, token, login, function(err,status){callback(err,status)});
                                    });
                                });
                            }
                        }
                    else{
                            User.send(JSON.stringify(
                                {
                                    status: "notification",
                                    data: "Извините, но на данного пользователя не заполнена информация"
                                }));
                            callback(null,'Нет инфы');
                        }

                    });
                } else {
                    // Если пароль неверен
                    User.send(JSON.stringify(
                        {
                            status: "notification",
                            data: "Пароль неверен!"
                        }));
                    callback(null,'Неправильный пароль');
                    console.log(now.getHours() + ':' + now.getMinutes() + " Неправильный пароль для логина - ".red + login);
                }
            } else {
                // Если такого логина нету в базе
                User.send(JSON.stringify(
                    {
                        status: "notification",
                        data: "Такого пользователя не существует!"
                    }));
                callback(null,'Нет логина');
                console.log(now.getHours() + ':' + now.getMinutes() + " Отсутствие пользователя".red);
            }
        } catch(e) {
            //console.log(now.getHours() + ':' + now.getMinutes() + ' ОШИБКА!!! - '.red + e);
            User.send(JSON.stringify(
                {
                    status: "notification",
                    data: "Такого пользователя не существует!"
                }));
            callback(null,'Нет пользователя');
            console.log(now.getHours() + ':' + now.getMinutes() + " Отсутствие пользователя".red);
        };
    });
};
// Регитрация нового пользователя
var Register = function (User,login,password,nStudentCard,dir,email) {
    now = new Date();
    // Соль
    var salt = Math.random() + ' nark';
    // Параметры процедуры
    var inParams=[nStudentCard, login, encryptPassword(password,salt), salt, email,'card-'+nStudentCard+' ;password- '+ password];
    var sqlstr='SELECT insert_new_user(?,?,?,?,?,?)';
    // Вызов процедуры бд
    connection.query(sqlstr, inParams, function (err,rows){
        if(!err){
            for (var name in rows[0]) {
                // Проверяем, что нам вернула процедура
                if (rows[0][name] == 0) {
                    // Если такой пользователь уже есть, то отсылаем ошибку
                    User.send(JSON.stringify(
                        {
                            status: "notification",
                            data: "Такой пользователь уже существует!"
                        }));
                }
                else if (rows[0][name] == 1){
                    // Если нет ошибок, выполняем авторизацию
                    Check(User,login,password,dir,function(){});
                }
                else {
                    // Если нет такого номера студенческого
                    User.send(JSON.stringify(
                        {
                            status: "notification",
                            data: "Такой номер студенческого билета не существует!"
                        }));
                }
            }
        }
        else {
            console.log(now.getHours() + ':' + now.getMinutes() + " " + err);
            User.send(JSON.stringify(
                {
                    status: "notification",
                    data: "Ошибка! Данный email уже занят! |" + err + "|"
                }));
        }
    });
};
// Выход пользователя из системы
var LogOut = function(login){
    var cryptLogin = encryptPassword(login,'nark');
    rediska.del(cryptLogin);
};
var render = function(User,dir,_data,token,login,callback){
    try{
        rediska.get('script' , function (err, script) {
            rediska.get('style' , function (errs, style) {
                var data = JSON.stringify(_data);
                var just = new JUST({ root : dir + '/view', useCache : true, ext : '.html' });
                just.render('page', {d:data}, function(error, html) {
                    if(!error){
                        var tmp = html.replace('﻿','');
                        User.send(JSON.stringify(
                            {
                                status: "OK",
                                token: token,
                                login: login,
                                data: tmp,
                                script: script,
                                style : style,
                                group: _data.group_id,
                                id: _data.ID
                            }));
                        callback(null,'Построили страницу');
                    }
                    else{
                        User.send(JSON.stringify(
                            {
                                status: "notification",
                                data: "Что то пошло не так:( Но мы скоро всё исправим;)"
                            }));
                        console.log('Ошибка построения - ' + error);
                        callback('error',null);
                    }
                });
            });
        });
    }catch(e){
        callback('error',null);
    };
}
// Проверка токена
var CheckToken = function (User,login,token,dir,callback) {
    now = new Date();
    var cryptLogin = encryptPassword(login,'nark');
    // Получаем токен для данного логина из кэша
    rediska.hmget(cryptLogin , 'token' , function (err, repl) {
        if(err){callback('err',null);}
        else{
            //Если есть правильный токен то все ок!
            if(repl == token){
                User.token = token;
                rediska.hlen(cryptLogin, function(err,count){
                    if(count > 1){
                        // Рендерим шаблон
                        rediska.hmget(cryptLogin , 'description' , function (err, description) {
                            var data = JSON.parse(description[0]);
                            User.id = data.ID;
                            User.name = data.fio;
                            console.log(now.getHours() + ':' + now.getMinutes() + ' Строим страницу после проверки токена');
                            render(User,dir,data,token,login,function(){callback});
                        });
                    }
                    else{
                        User.send(JSON.stringify(
                            {
                                status: "notification",
                                data: "Извините, но на данного пользователя не заполнена информация"
                            }));
                        callback(null,null);
                    }
                });
            }
            // Если кэш не найден или не cовпадает, то отправляем на страницу логина
            else{
                rediska.get('login-page' , function (err, repl) {
                    User.send(repl);
                });
                callback(null,null);
            }
        }
    });
};
// Функция зашифровки пароля
encryptPassword = function(password,salt) {
    return crypto.createHmac('sha1', salt).update(password).digest('hex');
};
// Экпорт функций
getBestOfGroup = function(group_id, callback){
    rediska.get('bestOfGroup_'+group_id, function (err, repl) {
        if (!err && repl) {
            return callback(null, JSON.parse(repl));
        }
        else {
            var sqlstr = 'call get_best_of_group(?)';
            connection.query(sqlstr, group_id, function (err, row_best_group) {
                var best_group = row_best_group[0];
                rediska.set('bestOfGroup_'+group_id, JSON.stringify(best_group));
                rediska.expire('bestOfGroup_'+group_id,300);
                return callback(null, best_group);
            });
        }
    });
}
getFullUserDescriptions = function(login,callback){
    // Получаем полную информацию по пользователю
    var sqlstr='call get_full_user_descriptions(?)';
    connection.query(sqlstr, login, function (err,rows) {
        if(!err) {
            var description = rows[0][0];
            callback(null, description);
        }
        else callback(err, null);
    });
};
getBest50 = function(callback){
    rediska.get('best50', function (err, repl) {
        if (!err && repl) {
            return callback(null, JSON.parse(repl));
        }
        else {
            var sqlstr = 'call get_best50()';
            connection.query(sqlstr, function (errorss, row_best) {
                if (!errorss) {
                    rediska.set('best50', JSON.stringify(row_best[0]));
                    rediska.expire('best50',300);
                    return callback(null, row_best[0]);
                }
                else {
                    return callback(errorss, null);
                }
            });
        }
    });
};
getScheduleForGroup = function(group_id,callback){
    rediska.get('shedule_'+ group_id , function (err, repl) {
        if(!err && repl){
            return callback(null,JSON.parse(repl));
        }
        else{
            sqlstr='call get_schedule_for_group(?,?)';
            var inParams=[group_id, new Date().getWeek()];
            connection.query(sqlstr, inParams, function (errors,row_schedule) {
                if(!errors){
                    rediska.set('shedule_'+ group_id,JSON.stringify(row_schedule[0]));
                    return callback(null,row_schedule[0]);
                }
                else{
                    return callback(errors,null);
                }
            });
        }
    });
};

module.exports.Check = Check;
module.exports.CheckToken = CheckToken;
module.exports.Register = Register;
module.exports.LogOut = LogOut;